import { userStorageKey, columnsStorageKey } from './initialize-api-storage';

export const getUserFromStorage = () => {
  const user = localStorage.getItem(userStorageKey);

  return JSON.parse(user);
};

const setUserToStorage = (user) => {
  localStorage.setItem(userStorageKey, JSON.stringify(user));
};

export const getColumnsFromStorage = () => {
  const columns = localStorage.getItem(columnsStorageKey);

  return JSON.parse(columns);
};

const setColumnsToStorage = (columns) => {
  localStorage.setItem(columnsStorageKey, JSON.stringify(columns));
};

export const addColumnToStorage = (column) => {
  const columns = getColumnsFromStorage();

  const newColumns = [...columns, column];
  setColumnsToStorage(newColumns);

  return newColumns;
};

export const removeColumnFromStorage = (columnId) => {
  const columns = getColumnsFromStorage();

  const columnToDelete = columns.find((c) => c.id === columnId);

  if (!columnToDelete) {
    throw new Error(`Колонка с id ${columnId} не найдена`);
  }

  const newColumns = columns.filter((c) => c.id !== columnId);
  setColumnsToStorage(newColumns);

  return columnToDelete;
};

export const addCardToStorage = (columnId, card) => {
  const columns = getColumnsFromStorage();

  const targetColumn = columns.find((c) => c.id === columnId);

  if (!targetColumn) {
    throw new Error(`Колонка с id ${columnId} не найдена`);
  }

  const updatedColumn = {
    ...targetColumn,
    cards: targetColumn.cards ? [...targetColumn.cards, card] : [card],
  };

  const updatedColumns = columns.map((c) =>
    c.id === updatedColumn.id ? updatedColumn : c
  );

  setColumnsToStorage(updatedColumns);
};

export const removeCardFromStorage = (cardId) => {
  const columns = getColumnsFromStorage();

  const targetColumn = columns.find((c) =>
    c.cards?.find((card) => card.id === cardId)
  );

  if (!targetColumn) {
    throw new Error(`Колонка для карточки с ID ${cardId} не найдена`);
  }

  const updatedColumn = {
    ...targetColumn,
    cards: targetColumn.cards.filter((card) => card.id !== cardId),
  };

  const updatedColumns = columns.map((c) =>
    c.id === updatedColumn.id ? updatedColumn : c
  );

  setColumnsToStorage(updatedColumns);
};
